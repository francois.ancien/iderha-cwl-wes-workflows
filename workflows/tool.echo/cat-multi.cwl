cwlVersion: v1.0
class: CommandLineTool

hints:
  - class: DockerRequirement
    dockerPull: alpine

baseCommand: cat
stdout: test-echo.txt

inputs:
  - id: input
    type: File
    inputBinding:
      position: 1
  - id: input2
    type: File
    inputBinding:
      position: 2
      
outputs: 
  example_out:
    type: stdout
