cwlVersion: v1.0
class: CommandLineTool
baseCommand: cat
stdout: test-cat.txt

inputs:
  - id: input
    type: File
    inputBinding:
      position: 1
outputs: 
  output_file:
    type: stdout
