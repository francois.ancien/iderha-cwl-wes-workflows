cwlVersion: v1.0
class: CommandLineTool

hints:
  - class: DockerRequirement
    dockerPull: iderha/does-not-exist

baseCommand: echo
stdout: test-echo.txt

inputs:
  message:
    type: string
    inputBinding:
      position: 1
outputs: 
  example_out:
    type: stdout
